package com.tesltra.rest.fibonacci.controller;



import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tesltra.rest.fibonacci.entity.FibonacciResult;
import com.tesltra.rest.fibonacci.service.FibonacciService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.emc.rubicon.rest.ErrorResponse;

@RestController
@Description("A controller for return fibonacci series")
public class FibonacciController {
    @Autowired
    private FibonacciService fibonacciService;

    @RequestMapping(value = "api/Fibonacci", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public FibonacciResult fibonacci(@RequestParam(value = "n", defaultValue = "1") String parm) {
            int num = Integer.valueOf(parm);
            return fibonacciService.fibonacci(num);
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<?> handleException(Exception e) {
        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            ErrorResponse response = new ErrorResponse("400", e.getMessage());
            json = mapper.writeValueAsString(response);
        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return new ResponseEntity<Object>(json + "\n", HttpStatus.BAD_REQUEST);
    }
}
