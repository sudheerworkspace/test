package com.tesltra.rest.fibonacci.entity;

import java.util.Arrays;


public class FibonacciResult {
    String number;
    String[] array;

   

    public String[] getArray() {
        return array;
    }

    public void setArray(String[] array) {
        this.array = array;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "FibonacciResult{" +
                "number='" + number + '\'' +
                ", array=" + Arrays.toString(array) +
                "}\n";
    }
}
