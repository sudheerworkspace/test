
package com.telstra.rest.fibonacci.app;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.*"})
public class FibonacciApplication {	
//commented for git
	public static void main(String[] args) throws Exception {
		SpringApplication.run(FibonacciApplication.class, args);
	}
	//https://github.com/dapenghu/fibonacci-rest

}
